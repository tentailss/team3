package com.team3.Team3.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "roles")
public class Role {
	  @Id
	  @GeneratedValue(strategy = IDENTITY)
	private int id;
	private String name;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "roleid")
	 private Set<UsersRoles> usersRoleses = new HashSet<UsersRoles>(0);
	public Role() {
		
	}
	public Role(String name, Set<UsersRoles> usersRoleses) {
		this.name = name;
		this.usersRoleses = usersRoleses;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<UsersRoles> getUsersRoleses() {
		return usersRoleses;
	}
	public void setUsersRoleses(Set<UsersRoles> usersRoleses) {
		this.usersRoleses = usersRoleses;
	}
	
}
