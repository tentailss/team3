package com.team3.Team3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team3.Team3.dao.UserDAO;
import com.team3.Team3.entities.User;
import com.team3.Team3.service.ServiceResult.Status;



@Service
@Transactional
public class UserService {
	@Autowired
	UserDAO userDao;

	@Autowired
	private JwtService jwtService;
	
	public ServiceResult getUserByName(String name) {
		ServiceResult serviceResult = new ServiceResult();
		String result;
		
		serviceResult.setData(userDao.getUserByName(name));
		User user = (User) serviceResult.getData();
		if(serviceResult.getData()!=null) {
			result = jwtService.generateTokenLogin(user.getName());
			serviceResult.setMessage(result);
			return serviceResult;
		}else {
			serviceResult.setStatus(Status.FAILED);
			serviceResult.setMessage("error");
			return serviceResult;
		}
		
	}
}
