package com.team3.Team3.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import static javax.persistence.GenerationType.IDENTITY;
@Entity
@Table(name = "users_roles")
public class UsersRoles {
	
	 @Id
	 @GeneratedValue(strategy = IDENTITY)
	private Integer id;
	 
	 @ManyToOne(fetch = FetchType.EAGER)
	  @JoinColumn(name = "userid")
	  private Role userid;
	 @ManyToOne(fetch = FetchType.EAGER)
	  @JoinColumn(name = "roleid")
	  private User roleid;
	  
	  public UsersRoles() {
	  }

	public UsersRoles( Role userid, User roleid) {
	
		this.userid = userid;
		this.roleid = roleid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Role getUserid() {
		return userid;
	}

	public void setUserid(Role userid) {
		this.userid = userid;
	}

	public User getRoleid() {
		return roleid;
	}

	public void setRoleid(User roleid) {
		this.roleid = roleid;
	}
	 

}
