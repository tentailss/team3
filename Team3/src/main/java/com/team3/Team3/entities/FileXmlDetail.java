package com.team3.Team3.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "filexmldetail")
public class FileXmlDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String beginline;
	private String endline;
	private String priority;
	private String content;
	private int begincolumn;
	private int endcolumn;
	private String rule;
	private String ruleset;
	@Column(name = "package")
	private String pack;
	@Column(name = "class")
	private String cla;
	private String externalinfourl;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idfile", nullable = false)
	private FileXML idfile;

	public FileXmlDetail( String name, String beginline, String endline, String priority, String content,
			int begincolumn, int endcolumn, String rule, String ruleset, String pack, String cla,
			String externalinfourl, FileXML idfile) {
	
		this.name = name;
		this.beginline = beginline;
		this.endline = endline;
		this.priority = priority;
		this.content = content;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.ruleset = ruleset;
		this.pack = pack;
		this.cla = cla;
		this.externalinfourl = externalinfourl;
		this.idfile = idfile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeginline() {
		return beginline;
	}

	public void setBeginline(String beginline) {
		this.beginline = beginline;
	}

	public String getEndline() {
		return endline;
	}

	public void setEndline(String endline) {
		this.endline = endline;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getBegincolumn() {
		return begincolumn;
	}

	public void setBegincolumn(int begincolumn) {
		this.begincolumn = begincolumn;
	}

	public int getEndcolumn() {
		return endcolumn;
	}

	public void setEndcolumn(int endcolumn) {
		this.endcolumn = endcolumn;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getRuleset() {
		return ruleset;
	}

	public void setRuleset(String ruleset) {
		this.ruleset = ruleset;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getCla() {
		return cla;
	}

	public void setCla(String cla) {
		this.cla = cla;
	}

	public String getExternalinfourl() {
		return externalinfourl;
	}

	public void setExternalinfourl(String externalinfourl) {
		this.externalinfourl = externalinfourl;
	}

	public FileXML getIdfile() {
		return idfile;
	}

	public void setIdfile(FileXML idfile) {
		this.idfile = idfile;
	}
	
	
}
