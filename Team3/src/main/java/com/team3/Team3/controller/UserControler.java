package com.team3.Team3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.team3.Team3.entities.User;
import com.team3.Team3.service.ServiceResult;
import com.team3.Team3.service.UserService;
import com.team3.Team3.service.ServiceResult.Status;

@RestController
public class UserControler {
	@Autowired
	UserService userService;
	
@PostMapping("/login")
public ResponseEntity<ServiceResult> login (@RequestBody User userRequest) {
	User user = (User) userService.getUserByName(userRequest.getName()).getData();
	
	
		if(user != null && user.getName().equals(userRequest.getName())&& user.getPassword().equals(userRequest.getPassword())) {
			return new ResponseEntity<ServiceResult>(userService.getUserByName(userRequest.getName()),HttpStatus.OK);
		}
		else {
			ServiceResult result = new ServiceResult();
			if(null == user) {
				result.setStatus(Status.FAILED);
				result.setMessage("username not exist");
				return new ResponseEntity<ServiceResult>(result,HttpStatus.OK);
			}
			
			if(!user.getPassword().equals(userRequest.getPassword())) {
				result.setStatus(Status.FAILED);
				result.setMessage("wrong username or  password");
				return new ResponseEntity<ServiceResult>(result,HttpStatus.OK);
			}
				
			result.setStatus(Status.FAILED);
			result.setMessage("error");
			return new ResponseEntity<ServiceResult>(result,HttpStatus.OK);
		}
}
@GetMapping("/rest/test")
public ResponseEntity<String> test () {
	return new ResponseEntity<String>("test",HttpStatus.OK);
}
}