package com.team3.Team3.dao;



import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.team3.Team3.entities.User;



@Repository
@Transactional(rollbackFor = Exception.class)
public class UserDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	public User getUserByName(String name) {
		try {
			 Query query = sessionFactory.getCurrentSession().createQuery("from User where name = :name");
			  query.setParameter("name", name);
			  return (User) query.list().get(0);
		} catch (Exception e) {
			return null;
		
	}
}
}